var URL = require('url');
var jwt = require('../scripts/jwt');
var strictUriEncode = require('strict-uri-encode');
var listActivityType = [
  {id: 1, name: 'Acúatica'},
  {id: 2, name: 'Avistaje de aves'},
  {id: 3, name: 'Cabalgatas'},
  {id: 4, name: 'Camping'},
  {id: 5, name: 'Ciclismo'},
  {id: 6, name: 'Deportes Extremos'},
  {id: 7, name: 'Deportes Invernales'},
  {id: 8, name: 'Expediciones/Travesías'},
  {id: 9, name: 'Montañismo y Trekking'},
  {id: 10, name: 'Pesca'},
  {id: 11, name: 'Tours/Otros'}
]


exports.sha1 = function(string){
	return require('crypto').createHash('sha1').update(string).digest("hex");
}

exports.appendParamToUrl = function(url,param){
	var u = URL.parse(url,true);
	u.search = '';
	u.query[param.name] = param.value;

	return URL.format(u);
}

exports.validatePassword = function(password){
	return password && (password.length >= 6 && password.length <= 20);
}

exports.decodeOrigin = function(origin){
	if(origin){
			return jwt.decode(origin);
	}else{
		return origin;
	}
} 

exports.normalizeNames = function(object) {
	for(var i=0; i<object.length; i++){
		if(object[i].name){
			object[i].safeUrlName = this.normalizeName(object[i].name.replace(/ /g, '-')); 
		}
	}
} 

exports.normalizeName = function(name) {
	return strictUriEncode(name.replace(/ /g, '-')); 
}

exports.getActivityType = function(activityId){
	var activityReturn;
	var exists = listActivityType.some(function(currentList){
    	if(currentList.id == activityId){
    		activityReturn = currentList;
    		return true;
    	}
    	return false;
    });

    if(exists){
    	return activityReturn;
    }
    return;
}

exports.getFormattedAddress = function (golocation) {
    var loc = '';

    if (golocation.address_components !== undefined) {
        if ((golocation.address_components.hasOwnProperty("administrative_area_level_1") === true) && 
           (golocation.address_components.hasOwnProperty("locality") === true)) {
 
            if(golocation.address_components.locality){
                loc += golocation.address_components.locality + ', ';
            }

            if(golocation.address_components.administrative_area_level_1){
                loc += golocation.address_components.administrative_area_level_1;
            }

            if (golocation.address_components.hasOwnProperty("country") === true) {
                loc += ', ' + golocation.address_components.country;
            }

            return loc;
        }
    }
    
    return loc;
}
