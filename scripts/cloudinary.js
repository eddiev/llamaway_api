var cloudinary = require('cloudinary');
var q = require('q');
var _ = require('lodash');

var cloud_name = process.env.CLOUDINARY_APP_NAME || "dhp5jgxth" ;
var api_key = process.env.CLOUDINARY_KEY || "152734631884186";
var api_secret = process.env.CLOUDINARY_SECRET|| "bSaKVemvxTSruaGfB1PFBk2Dli8";

cloudinary.config({ 
  cloud_name: cloud_name, 
  api_key: api_key, 
  api_secret: api_secret 
});

// Chequear si width y height  "consumen" una transformación
var defaultOptions = { 
	crop: 'limit', 
	width: 1000, 
	height: 1000, 
	tags: ['llamaway','company_profile_picture'] 
};

exports.uploadPicture = function(path,moreOptions){
	var deferred = q.defer();
	var opts = _.assign(defaultOptions,moreOptions) 
	cloudinary.uploader.upload(path, deferred.resolve, opts);
	return deferred.promise
};

exports.uploadFile = function(path,moreOptions){
	var deferred = q.defer();
	
	var opts = _.assign({
		resource_type:'raw'
	},moreOptions);

	cloudinary.uploader.upload(path, deferred.resolve,opts);
	return deferred.promise
};

exports.deletePicture = function(publicId){
	var deferred = q.defer();
	cloudinary.uploader.destroy(publicId, function(res) { 
		if(res.result == 'ok'){
			deferred.resolve();
		} else {
			deferred.reject();
		}
	});

	return deferred.promise
};



