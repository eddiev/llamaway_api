var path = require("path");
var q = require("q");
var mustache = require("mustache");
var nodemailer = require("nodemailer");
var smtpTransport = require('nodemailer-sendgrid-transport');
var readFile = require('fs-readfile-promise');

var validator = require("validator");

var emailTemplatesDir = path.resolve(__dirname,'../templates/emails');
var constants = require('../templates/emails/constants');

var smtpTransport = nodemailer.createTransport(smtpTransport({
    host:"smtp.sendgrid.com", 
    port: 587,
    auth: {
        api_key: "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"
    }
}));

exports.sendRecoverPassword = function (data,cb) {
	var that = this;

	this.loadTemplateInLayout('recover_password',data).then(function(html){
		that.send(data.email, 'Restablece tu contraseña', html, cb);
	});
}

exports.sendWelcome = function (data,cb) {
	var that = this;
	
	this.loadTemplateInLayout('welcome_user',data).then(function(html){
		that.send(data.email, '¡Ya estás registrado, ahora elige tu propia aventura!', html, cb);
	})
}

exports.sendWelcomeCompany = function (data,cb) {
	var that = this;
	
	this.loadTemplateInLayout('welcome_company',data).then(function(html){
		that.send(data.email, 'Registro exitoso', html, cb);
	})
}

exports.send = function(to, subject, html, cb){
	this.sendCustomEmail("Llamaway <no_responder@llamaway.com>", to, subject, html, cb);
}

exports.sendCustomEmail= function(from, to, subject, html, cb){
	cb = cb || function(){};
	var fromEmail = from || "Llamaway <no_responder@llamaway.com>";
	var mailOptions = {
	    from: fromEmail, // sender address
	    to: to, 
	    subject: subject,	    
	    html: html
	}

	smtpTransport.sendMail(mailOptions, function(err, info){
		if (err){
			console.log(err);
		} else {
			cb();
		}
	});

}

exports.loadTemplateInLayout = function(templateName,data){
	var promises = [];
	promises.push(readFile(path.resolve(emailTemplatesDir,'layout.html')));
	promises.push(readFile(path.resolve(emailTemplatesDir,templateName+'.html')));

	data.constants = constants;

	return q.all(promises).then(function(responses){
		var layout = responses[0].toString();
		
		var partials = {
			content:responses[1].toString()
		}
		
		return mustache.render(layout,data,partials);
	})	
}

exports.validateFormat = function(email){
	return validator.isEmail(email);
}

exports.sendEmailValidation = function (data,cb) {
    var that = this;

    data.footer = ''; //'https://www.glinzz.com/?utm_source=recommend&utm_medium=email&utm_campaign=footer';
    
    this.loadTemplateInLayout('email_validation',data).then(function(html){
        that.send(data.email, 'Verificación de tu e-mail', html, cb);
    })
}

exports.sendContactEmail = function (data,cb) {
    var that = this;

    data.footer = ''; //'https://www.glinzz.com/?utm_source=recommend&utm_medium=email&utm_campaign=footer';
    
    this.loadTemplateInLayout('email_contact', data).then(function(html){
        that.send(data.email, 'Recibiste una nueva consulta', html, cb);
    })
}

exports.sendContactEmailLlamita = function (data,cb) {
    var that = this;

    this.loadTemplateInLayout('email_contact_llamita', data).then(function(html){
        that.send(data.email, 'Contactaste a ' + data.companyName, html, cb);
    })
}

exports.sendLowLeadsEmail = function (data,cb) {
    var that = this;

    data.footer = ''; //'https://www.glinzz.com/?utm_source=recommend&utm_medium=email&utm_campaign=footer';
    
    this.loadTemplateInLayout('email_low_leads', data).then(function(html){
        that.send(data.email, 'Te quedan pocas consultas', html, cb);
    })
}


exports.sendPendingLeadEmailLlamita = function (data,cb) {
    var that = this;

    this.loadTemplateInLayout('email_pending_lead_llamita', data).then(function(html){
        that.send(data.email, 'Contactaste a ' + data.companyName, html, cb);
    })
}

exports.sendPendingLeadEmail = function (data,cb) {
    var that = this;

    data.footer = ''; //'https://www.glinzz.com/?utm_source=recommend&utm_medium=email&utm_campaign=footer';
    
    this.loadTemplateInLayout('email_pending_lead', data).then(function(html){
        that.sendCustomEmail("Llamaway <comercial@llamaway.com>", data.email, 'Te quedaste sin consultas', html, cb);
    })
}