var jwt = require('jwt-simple');
var secretString = '_ÑÄLSD**RATratRAT**_ÑÄLSD';

exports.encode = function(data){
	return jwt.encode(data, secretString);
}

exports.decode = function(data){
	return jwt.decode(data, secretString);
}