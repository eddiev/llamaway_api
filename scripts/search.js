var algoliasearch = require('algoliasearch');
var q = require('q');
var _ = require('lodash');

var utils = require('./utils');

var sandBoxAppId = process.env.ALGOLIA_APP_ID || "DH79J7IGKQ" ;
var sandBoxKey = process.env.ALGOLIA_KEY || "372091ee45679a912815813358a31fe5";

var client = algoliasearch(sandBoxAppId, sandBoxKey);

var adventureIndex = client.initIndex('adventures');

var companyIndex = client.initIndex('companies');

adventureIndex.setSettings({
    attributesToIndex: ['activityType.name', 'golocation.formatted_address', 'name'],
    attributesForFaceting: ['_id','price', 'activityType.name', 'activityType.id', 'golocation.address_components.administrative_area_level_1', 'difficulty', 'duration'],
    customRanking: ['desc(name)']
});

companyIndex.setSettings({
    attributesToIndex: ['golocation.formatted_address', 'activities.id', 'name'],
    attributesForFaceting: ['activities.id', 'golocation.address_components.administrative_area_level_1'],
    customRanking: ['desc(name)']
});

var indexObj = function(obj, index){
	obj.objectID = obj._id;

    index.partialUpdateObject(obj, function(err, content) {
        if(err){
            console.error('err index', obj._id,err);
            return;
        }else{
        	console.error('index ok', obj._id);
        }
    });
}

var deleteObj = function(objId, index) {
    index.deleteObject(objId, function(err) {
        if(err){
            console.error('Error deleting the object fron index', objId, err);
        }else{
        	console.log('Object removed from the index ' + objId);
        }
    });
}

var searchObj = function(index, query, restrictions){

	var deferred = q.defer();

	var queryQ = '';

	if(query.name && query.name.length > 0){
		queryQ = query.name;
	}

	var options = {
		filters: '',
		facets: '*',
		aroundLatLng: query.geoloc || '',
		page: query.page || 0,
	    hitsPerPage: query.limit || 30, 
	    removeWordsIfNoResults:'allOptional',
	    queryType:'prefixLast',
	    attributesToHighlight: null //Por ahora no lo necesito. Indicador de matching de palabras
	};

	if(query.withoutDescription){ // Se quitan description y _highlightResult
		options.attributesToRetrieve = ['__v', '_geoloc', '_id', 'activityType', 
										'adventurePictureCount','companyId', 'created_at', 'difficulty', 'currency',
										'duration', 'golocation', 'location', 'name', 'objectID', 'ownerId', 'price',
										'payMethod', 'place', 'profile_picture', 'safeUrlName', 'takeDays', 'updated_at'];
	}

	if(query.radius){
		options.aroundRadius = query.radius;
	}

	if(query.geoloc){
		options.getRankingInfo = true;
	}

	if(query.activity){
		var activityField = 'activityType.id';
		if(index.indexName == 'companies'){
			activityField = 'activities.id';
		}
		if(query.activity.indexOf(',') > -1){
			var filters = query.activity.split(',');
			var activityFilter = '(';
			filters.forEach(function(current, index){
				activityFilter += activityField + '=' + current;
				if(index < filters.length -1){
					activityFilter += ' AND ';
				}
			});
			options.filters = activityFilter + ')';
		}else{
			options.filters = activityField + '=' + query.activity;
		}
	}


	if(query.location){
		queryQ += ' ' + query.location;
		if(options.filters.length > 0){
			options.filters += ' AND ';
		}

		if(query.location.indexOf(',') > -1){
			var filters = query.location.split(',');
			var locationFilter = '(';
			filters.forEach(function(current, index){
				locationFilter += 'golocation.address_components.administrative_area_level_1:\"'+ decodeURI(current)+'\"';

				if(index < filters.length -1){
					locationFilter += ' OR ';
				}
			});
			options.filters += locationFilter + ')';
		}else{
			options.filters += 'golocation.address_components.administrative_area_level_1:\"'+ decodeURI(query.location)+'\"';
		}
	}

	if(query.difficulty){
		if(options.filters.length > 0){
			options.filters += ' AND ';
		}
		var difficultyField = 'difficulty';

		if(query.difficulty.indexOf(',') > -1){
			var filters = query.difficulty.split(',');
			var  difficulty = '(';
			filters.forEach(function(current, index){
				 difficulty += difficultyField + '=' + current;
				if(index < filters.length -1){
					 difficulty += ' AND ';
				}
			});
			options.filters += difficulty + ')';
		}else{
			options.filters += difficultyField + '=' + query.difficulty;
		}
	}

	if(query.duration){

		var filters = query.duration.split(',');

		if(filters.length > 0){
			if(options.filters.length > 0){
				options.filters += ' AND ';
			}
			options.filters += 'takeDays = ' +  filters[0] + ' AND (duration >= ' + filters[1] + ' AND duration <= ' + filters[2] + ')';
		}
	}

	if(query.price){
		var filters = query.price.split(',');

		if(filters.length > 0){
			if(options.filters.length > 0){
				options.filters += ' AND ';
			}
			var filtersPrice = filters[0].split('_');
			filtersPrice.forEach(function(current, index){
				if(index > 0){
					options.filters += ' AND ';
				}
				switch(current){
					case 'conprecio':
						var currency = 0;
						if(filters[1] != 'pesos'){
							currency = 1;
						}
						options.filters += '(currency = '+ currency +' AND price >= ' + filters[2] + ' AND price <= ' + filters[3] + ')' ;
					break;
					case 'gratuita':
						options.filters += 'price=0';
					break;
				}
			});
		}
	}

	if(query.adventures){
		if(options.filters.length > 0){
			options.filters += ' AND ';
		}
		var adventuresField = '_id';

		if(query.adventures.indexOf(',') > -1){
			var filters = query.adventures.split(',');
			var  adventures = '(';
			filters.forEach(function(current, index){
				 adventures += adventuresField + ':\"' + current + '\"';
				if(index < filters.length -1){
					 adventures += ' OR ';
				}
			});
			options.filters += adventures + ')';
		}else{
			options.filters += adventuresField + ':' + query.adventures;
		}
	}

	index.search(queryQ, options,function(err,res){
		if(err){
			deferred.reject(err);
		}else{
			deferred.resolve({
				count:res.nbHits,
				results:res.hits,
				page: res.page,
				nbPages: res.nbPages,
				hitsPerPage: res.hitsPerPage,
				processingTimeMS: res.processingTimeMS,
                facets: res.facets,
                facets_stats : res.facets_stats 
			});
		}
	});
	return deferred.promise;
	
}

exports.indexAdventure = function(adventure) {
	adventure.golocation.formatted_address = utils.getFormattedAddress(adventure.golocation);
	return indexObj(adventure, adventureIndex)
}

exports.deleteAdventure = function(adventureId) {
    return deleteObj(adventureId, adventureIndex);
}

exports.searchAdventure = function(query, restrictions){
	return searchObj(adventureIndex, query, restrictions);
}

exports.indexCompany = function(company) {
	if(company.golocation){
		company.golocation.formatted_address = utils.getFormattedAddress(company.golocation);
	}
	return indexObj(company, companyIndex)
}

exports.deleteCompany = function(companyId) {
    return deleteObj(companyId, companyIndex);
}

exports.searchCompany = function(query, restrictions){
	return searchObj(companyIndex, query, restrictions);
}