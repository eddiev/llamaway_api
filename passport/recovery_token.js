var util = require('util')
  , Strategy = require('passport-strategy');
var passport = require('passport');
var Auth = require('../models/auth');
var User = require('../models/user');
 
function RecoveryTokenStrategy() {
	this.name = 'recovery_token';
	Strategy.call(this);
}

RecoveryTokenStrategy.prototype.authenticate = function(req,options) {
	var that = this;
	var query = {
		email:req.query.email,
		recovery_token:req.query.recovery_token
	}

	Auth.findOne(query, function(err, userAuth) {
	    if (userAuth) {
	        User.findById(userAuth.uid, function(err, user) {
	            req.user = user;
	            that.pass();
                //Valido al usuario
                user.validated = true;
	            user.recovery_token = '';
	            user.save(function() {})
	        })
	    } else {
	        that.fail()
	    }

	})

};

passport.use(new RecoveryTokenStrategy());