var passport = require('passport');
var LinkedInStrategy = require('passport-linkedin').Strategy;
var User = require('../models/user');
var Auth = require('../models/auth');


passport.use(new LinkedInStrategy({
        consumerKey: '356363466346634',
        consumerSecret: '356363466346634',
        scope: ['r_basicprofile', 'r_emailaddress'],
        profileFields: ['id', 'first-name', 'last-name', 'email-address', 'headline', 'positions','picture-url', 'industry', 'summary', 'specialties', 'public-profile-url']
    },
    function(req, token, tokenSecret, profile, done) {
        req.userCreated = false;
        req.provider = 'linkedin';

        var linkedinData = {
            id: profile.id,
            email:profile.emails[0].value,
            accessToken: token,
            cachedProfile: profile._json
        }

        // buscamos por email o por provider id porque la primera vez matchea el email 
        // y las siguientes el id para que el usuario pueda cambiar el email

        User.findOne({
            $or:[{
                'linkedin.id': linkedinData.id
            },{
                'email': linkedinData.email
            }]
        }, function(err, user) {
            if(user){
                user.linkedin = linkedinData;
            }else{
                var data = {
                    linkedin: linkedinData,
                    email:linkedinData.email||''
                }
                var user = User.createUserAndAuth(data);

                req.userCreated = true;
            }
            
            if(!user.profile_picture){
                var newImage = new ImageModel({
                    host:'linkedin',
                    data:{
                        url:profile._json.pictureUrl
                    }
                })

                //async
                newImage.save(function(err){
                    if(err){
                        console.log('error creando imagen de perfil from linkedin');
                    }
                })
                
                user.profile_picture = {
                    imageId:newImage._id,
                    url:newImage.data.url
                }
            }

            user.first_name = user.first_name || profile.name.givenName;
            user.last_name = user.last_name || profile.name.familyName;

            user.save(function(err) {
                done(err, user);
            });
        });
    }
));