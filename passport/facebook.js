var passport = require('passport');
var FacebookStrategy = require('passport-facebook');
var User = require('../models/user');
var Auth = require('../models/auth');
var ImageModel = require('../models/image');


passport.use(new FacebookStrategy({
        clientID: '172645673106272',
        clientSecret: 'c8a7337225b20f9d87f4edbaf3c5c553',
        profileFields: ['id','name','email','picture','gender'],
        scope: ['email','public_profile'],
        enableProof: false,
        passReqToCallback:true
    },
    function(req,accessToken, refreshToken, profile, done) {
        if(!profile.emails){
            return done(null, {'profileError': profile.name.familyName + ' ' + profile.name.givenName});
        } else {
            req.userCreated = false;
            req.provider = 'facebook';
            console.log(profile)
            var facebookData = {
                id: profile._json.id,
                email:profile.emails[0].value,
                accessToken: accessToken,
                cachedProfile: profile._json
            }

             // buscamos por email o por provider id porque la primera vez matchea el email 
            // y las siguientes el id para que el usuario pueda cambiar el email
            
            User.findOne({
                $or:[{
                    'facebook.id': facebookData.id
                },{
                    'email': facebookData.email
                }]
            }, function(err, user) {
                console.log('req.query.sl');
                console.log(req.query.sl);
                if(user || req.query.sl != 'undefined'){

                    if(user){
                        user.facebook = facebookData;
                    }else{
                        var data = {
                            facebook: facebookData,
                            email:facebookData.email||''
                        }
                        var user = User.createUserAndAuth(data);

                        req.userCreated = true;
                    }

                    if(!user.profile_picture){
                        var newImage = new ImageModel({
                            host:'facebook',
                            data:{
                                url:profile.photos[0].value
                            }
                        })

                        //async
                        newImage.save(function(err){
                            if(err){
                                console.log('error creando imagen de perfil from facebook');
                            }
                        })
                        
                        user.profile_picture = {
                            imageId:newImage._id,
                            url:newImage.data.url
                        }
                    }


                    user.first_name = user.first_name || profile.name.givenName;
                    user.last_name = user.last_name || profile.name.familyName;

                    req.select = false;
                    user.save(function(err) {
                        done(err, user);
                    });
                }else{
                    req.select = true;
                    done(null, {});
                }
            });
        }
    }
));