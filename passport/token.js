var utils = require('../scripts/utils');
var Strategy = require('passport-strategy');
var passport = require('passport');
var Auth = require('../models/auth');
var User = require('../models/user');
 
function TokenStrategy() {
	this.name = 'token';
	Strategy.call(this);
}

TokenStrategy.prototype.authenticate = function(req,options) {
		console.log('auth');
	var that = this;
	
	var tok = req.jwt.decode(req.params.token);
	
	var query = {
		uid:tok._id
	}

	Auth.findOne(query,function(err,userAuth){
		if(userAuth){
			User.findById(userAuth.uid,function(err,user){
				req.user = user;
				that.pass();
			})
		}else{
			that.fail()
		}

	})
};

passport.use(new TokenStrategy());