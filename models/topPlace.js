require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var topPlaceSchema = new Schema({
	name:String,
	location:{
		address: String,
		coords: {
			lat: String,
			lng: String
		}
	},
	picture:[{
		imageId:Schema.Types.ObjectId,
		url:String
	}]
});

module.exports = mongoose.model('top_place', topPlaceSchema);
