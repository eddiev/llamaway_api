require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var backSchema = new mongoose.Schema({
	uid:{
		type:Schema.Types.ObjectId,
		index:true,
		unique:true
	}
});

module.exports = mongoose.model('backofficeUser', backSchema);
