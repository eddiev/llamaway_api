require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ImageModel = require('./image');
var cloudinary = require('../scripts/cloudinary');
var searchClient = require('../scripts/search');
var _ = require('lodash');

var adventureSchema = new Schema({
	_id:Schema.Types.ObjectId
},{strict: true});

var companySchema = new Schema({
	name:String,
	contactName:{
		type:String,
		select:false
	},
	pageName: String, 
	description:String,
	activities:	[{
		id:Number,
		name: String
	}],
	phone:{
		type:String,
		select:false
	},
	ownerId:{
		type: Schema.Types.ObjectId,
		select: false
	},
	adventures:{
		type:[adventureSchema],
		select:false
	},
	adventuresCount:Number,
	profile_picture:{
		imageId:Schema.Types.ObjectId,
		url:String
	},
	created_at:{
		type:Date,
		default:Date.now
	},
	updated_at:{
		type:Date,
		default:Date.now
	},
	leads: Number,
	golocation:{
	    formatted_address : String,
		address_components: {
	        administrative_area_level_1: String,
	        administrative_area_level_2: String,
	        locality: String,
	        country: String
		},
		location: {
			lat: Number,
			lng: Number
		} 
	},
	_geoloc: {
		lat: Number,
		lng: Number
	}	
})

companySchema.statics.uploadPicture = function(companyId,file,cb){
	var that = this;

	cloudinary.uploadPicture(file,{folder:'companies_profile_pictures'}).then(function(picture){
		var newImage = new ImageModel({
			host:'cloudinary',
			data:picture
		});

		newImage.save(function(err){
			var profile_picture = {
				imageId:newImage._id,
				url:newImage.data.secure_url
			}
			var update = { 
				$set: { 
					profile_picture: profile_picture 
				}
			}
			that.findByIdAndUpdate(companyId, update,{'new':true}, cb);
		});

	});
}

companySchema.statics.deletePicture = function(company,cb){
	var that = this;

	if(company.profile_picture && company.profile_picture.imageId){
		ImageModel.findOneAndRemove({'_id':company.profile_picture.imageId}, function(error, picture){
			if(picture && picture.data){
				cloudinary.deletePicture(picture.data.public_id).then(function(){
					cb();
				}, function (error) {
					cb('Error');
					console.log('Error en cloudinary, no se pudo remover la imagen: ' + picture.data.public_id);
				});
			} else {
				cb('Error no hay imagen');
			}
		})
	} else {
		cb();
	}
}

var Model = mongoose.model('Company', companySchema);


module.exports = Model;