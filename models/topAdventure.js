require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var topAdventuresSchema = new Schema({
	adventureId: Schema.Types.ObjectId
});

module.exports = mongoose.model('top_adventure', topAdventuresSchema);
