require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var packageSchema = new Schema({
	companyId: Schema.Types.ObjectId,
	dischargeLeads: Number,
	exposition: Number,
	price: Number,
	createdDate:{
		type:Date,
		default:Date.now
	},
	dischargeDate:Date,
	closeDate:Date,
	state:{
		type:Number,
		default:0
	},
	period: String //monthly - yearly
});

packageSchema.statics.availablePackage = function(companyId,cb){
	var that = this;

	var currentDate = new Date().toISOString();
	var option = {
	    $or:[{
	        state: 1,
	        exposition: {$in:[2,3,4]},
	        dischargeDate:{
	            $lte: currentDate
	        },
	        closeDate:{
	            $gte: currentDate
	        }  
	    },{
	        state: 1,
	        exposition: 1,
	        dischargeDate:{
	            $lte: currentDate
	        },
	        closeDate:{
	            $gte: currentDate
	        } 
	    }]
	}
    option.companyId = companyId;
    that.findOne(option, '', {'lean':true}, function(err, availablePackage){
        cb(err, availablePackage);
    });
}

var Model = mongoose.model('packages', packageSchema);
module.exports = Model;