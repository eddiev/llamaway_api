require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var _ = require('lodash');
var cloudinary = require('../scripts/cloudinary');
var q = require("q");
var ImageModel = require('./image');
//https://github.com/Automattic/mongoose/issues/1630

var indexName = 'llamaway';
var typeName = 'adventures';

var adventureSchema = new Schema({
	name:String,
	activityType:[{
		id:Number,
		name: String
	}],
	description:String,
	place:String,
	takeDays:Boolean,
	duration:Number,
	difficulty:Number,
	price:Number,
	startDate:Date,	
	finishDate:Date,
	companyId:Schema.Types.ObjectId,
	ownerId:{
		type: Schema.Types.ObjectId,
		select: false
	},
	created_at:{
		type:Date,
		default:Date.now
	},
	updated_at:{
		type:Date,
		default:Date.now
	},
	profile_picture:[{
		imageId:Schema.Types.ObjectId,
		url:String
	}],
	adventurePictureCount:Number,
	currency: Number,
	payMethod:[{
		id:Number,
		name: String
	}],
	golocation:{
	    formatted_address : String,
		address_components: {
	        administrative_area_level_1: String,
	        administrative_area_level_2: String,
	        locality: String,
	        country: String
		},
		location: {
			lat: Number,
			lng: Number
		} 
	},
	_geoloc: {
		lat: Number,
		lng: Number
	}	
});



adventureSchema.statics.uploadPicture = function(adventureId, file, oldPictureId){
	var that = this;

	var deferred = q.defer();

	cloudinary.uploadPicture(file,{folder:'adventure_profile_pictures'}).then(function(picture){
		var newImage = new ImageModel({
			host:'cloudinary',
			data:picture
		});

		newImage.save(function(err){
			if(err){
				deferred.reject(err);
			}else{
				var profile_picture = {
					'imageId':newImage._id,
					'url':newImage.data.secure_url
				}

				if(oldPictureId){
					// Si tengo que eliminar una imagen no puedo ejecutar pull y push sobre el mismo campo profile_picture
					// porque es una limitación de mongo. Tengo que ejecutar 2 operaciones atómicas

					var update = { 
						'$pull': {'profile_picture': 
							{
								'imageId':oldPictureId,
								'$inc':{'adventurePictureCount':-1}
							}
						}
					}

					that.findByIdAndUpdate(adventureId, update, function(err, aventure){
						if(err){
							deferred.reject(err);
						}else{
							// Ahora si agrego la imagen nueva
							that.findByIdAndUpdate(adventureId, {
								'$push': {'profile_picture': profile_picture, 
								'$inc':{'adventurePictureCount':1}}
							}, {'new':true}, function(err, aventure){

								if(err){
									deferred.reject(err);
								}else{
									deferred.resolve(aventure);
								}
							});
						}
					});
				} else {
					var update = { 
						'$push': {'profile_picture': profile_picture},
						'$inc':{'adventurePictureCount':1} 
					}
				}

				that.findByIdAndUpdate(adventureId, update, {'new':true}, function(err, aventure){
					if(err){
						deferred.reject(err);
					}else{
						deferred.resolve(aventure);
					}
				});
			}
		});

	}).catch(function(err){
		deferred.reject(err);
	});

	return deferred.promise;
}

adventureSchema.statics.deletePicture = function(adventureId, oldPictureId, cb){
	var that = this;

	if(oldPictureId){
		ImageModel.findOneAndRemove({'_id':oldPictureId}, function(error, picture){
			if(picture && picture.data){
				cloudinary.deletePicture(picture.data.public_id).then(function(){
					if(adventureId){
						var update = { 
							'$pull': {'profile_picture': 
								{
									'imageId':oldPictureId
								}
							},
							'$inc':{'adventurePictureCount':-1}
						}
						that.findByIdAndUpdate(adventureId, update, function(err, adv){
							if(err){
								cb({'err':err});
							}else{
								cb(adv);
							}
						});
					}else{
						cb();
					}
				}).catch(function (error) {
					cb({'err':'Error en cloudinary, no se pudo remover la imagen: ' + picture.data.public_id});
					console.log('Error en cloudinary, no se pudo remover la imagen: ' + picture.data.public_id);
				});
			} else {
				console.log('Error no hay imagen');
				cb({'err':'Error no hay imagen'});
			}
		}).catch(function(err){
			console.log('Error no hay imagen');
			cb({'err':'Error no hay imagen'});
		});
	} else {
		console.log('No hay id de imagen');
		cb({'err': 'No hay id de imagen'});
	}
}

module.exports = mongoose.model('Adventure', adventureSchema);
