require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var priceSchema = new Schema({
	monthly: Number,
	yearly: Number
})

var packageTemplateSchema = new Schema({
	title: String,
	description: String,
	leads: Number,
	state: Boolean,
	exposition:	Number,
	price: priceSchema,
	order:Number,
	mpId: String
});

var Model = mongoose.model('package_templates', packageTemplateSchema);
module.exports = Model;