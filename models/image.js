require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Ejemplo

// {
// 	host:'cloudinary',
// 	data:{ ... data de cloudinary ...}
// }

var imageSchema = new Schema({
	host:String,
	data:Schema.Types.Mixed
})


module.exports = mongoose.model('Image', imageSchema);
