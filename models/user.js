require('./db');
var mongoose = require('mongoose');
var _ = require('lodash');
var utils = require('../scripts/utils');
var Auth = require('./auth');

var Schema = mongoose.Schema;
var findOneOrCreate = require('mongoose-find-one-or-create');

var userSchema = new mongoose.Schema({
	email: {
		type: String,
		unique: true,
		select: true
	},
	companyId:{
		type:Schema.Types.ObjectId
	},
	created_at:{
		type:Date,
		default:Date.now
	},
	updated_at:{
		type:Date,
		default:Date.now
	},
	lastLogin:Date,
	validated:{
		type: Boolean, 
		default:false
	},
	notificationConfig:{
		messageDaysFrequency:{
			type: Number, 
			default:1
		}
	}
});

userSchema.statics.clean = function(obj){
	var attrNames = ['email'];
	return _.pick(obj, attrNames);	
}

userSchema.statics.createUserAndAuth = function(options,cb){
	cb = cb || function(){};

	var newUser = new this(options);


	newUser.save(function(err){
		if(err){
			cb(err)
		}else{
			console.log('Usuario de base creado')
			var newAuth = new Auth(options);
			newAuth.uid = newUser._id;
			newAuth.recovery_token = utils.sha1((Math.random()*1234)+options.email);
			newAuth.recovery_token_created_at = Date.now();
			newAuth.save(function(err){
				console.log('Oauth creado')
				cb(err, newUser, newAuth);
			})
		}
	})

	return newUser;
}

userSchema.statics.updateLastLogin = function(id, cb){
	cb = cb || function(){};

	this.findOneAndUpdate({
		_id: id
	}, {'lastLogin': new Date()}, function() {
		cb();
	});
}

userSchema.plugin(findOneOrCreate);


module.exports = mongoose.model('User', userSchema);
