require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var leadSchema = new Schema({
	userId:{
		type:Schema.Types.ObjectId
	},
	companyId:{
		type:Schema.Types.ObjectId
	},
	adventureId:{
		type:Schema.Types.ObjectId
	},
	contactDate:{
		type:Date,
		default:Date.now
	},
	phone:String,
	email: String,
	contactName: String,
	phone: String,
	description: String,
	state: {type: String, enum: ['sended', 'pending'], default: 'sended'}
})

var Model = mongoose.model('Lead', leadSchema);
module.exports = Model;