require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var authSchema = new mongoose.Schema({
	uid:{
		type:Schema.Types.ObjectId,
		index:true,
		unique:true
	},
	email:{
		type:String,
		index:true,
		unique:true
	},
	password:String,
	recovery_token:String
});

module.exports = mongoose.model('Auth', authSchema);
