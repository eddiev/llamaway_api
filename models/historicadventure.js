require('./db');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var _ = require('lodash');
var cloudinary = require('../scripts/cloudinary');
var q = require("q");
var ImageModel = require('./image');
//https://github.com/Automattic/mongoose/issues/1630

var indexName = 'llamaway';
var typeName = 'historicadventures';

var historicadventureSchema = new Schema({
	adventureId:Schema.Types.ObjectId,
	name:String,
	activityType:[{
		id:Number,
		name: String
	}],
	description:String,
	location:{
		address: String,
		coords: {
			lat: String,
			lng: String
		} 

	},
	place:String,
	takeDays:Boolean,
	duration:Number,
	difficulty:Number,
	price:Number,
	startDate: Date,	
	finishDate: Date,
	companyId:Schema.Types.ObjectId,
	ownerId:{
		type: Schema.Types.ObjectId,
		select: false
	},
	created_at: Date,
	updated_at: Date,
	profile_picture:[{
		imageId:Schema.Types.ObjectId,
		url:String
	}],
	adventurePictureCount:Number,
	currency: Number,
	payMethod:[{
		id:Number,
		name: String
	}],
	_geoloc: {
		lat: Number,
		lng: Number
	},
	created_at_history:{
		type:Date,
		default:Date.now
	},
});

module.exports = mongoose.model('historicadventure', historicadventureSchema);
