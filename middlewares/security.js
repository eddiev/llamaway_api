var sanitize = require("mongo-sanitize");

var security = function(req, res, next) {
    _mongoInjection(req, res, next);
}

var _mongoInjection = function(req, res, next) {
    req.body = sanitize(req.body);
    req.params = sanitize(req.params);
    req.query = sanitize(req.query);

    next();
}



module.exports = exports = security;
