module.exports = function(req,res,next){
	if(req._user){
		next();
	}else{
		res.status(401).json({statusCode:401});
	}
}