var BackofficeUser = require('../models/backofficeUser');

module.exports = function(req,res,next){
	if(req._user){
		console.log(req._user._id);
		BackofficeUser.findOne({'uid':req._user._id}, function(err, doc){
			if(err){
				console.error(err);
				res.status(500).json({statusCode:500});
			}else{
				if(doc){
					next();
				}else{
					res.status(401).json({statusCode:401});
				}
			}
		});
	}else{
		res.status(401).json({statusCode:401});
	}
}