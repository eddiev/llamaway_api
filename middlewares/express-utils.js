var url = require('url');
var jwt = require('../scripts/jwt');

module.exports.success = function(req,res,next){
	// location

	var proto =  req.headers['x-forwarded-proto'] || (req.connection.encrypted ? 'https' : 'http');
	req.fullURL = proto + '://' + req.headers.host + req.url;
	req._location =  url.parse(req.fullURL,true);
	req._location.origin = req._location.protocol + '//' + req._location.host

	console.log('req.fullURL',req.method,req.fullURL);
	// bearer

	if(req.headers.authorization && 
	   req.headers.authorization.split(' ')[0] == 'Bearer'){

	   req._bearer = req.headers.authorization.split(' ')[1];
	}

	// jwt
	req.jwt = jwt;

	if(req.body){
		// remove any _id in any body
		// avoid receive ids to override it
		delete req.body._id;
	}

	// user_id
	if(req._bearer){
		try{
			req._user = jwt.decode(req._bearer);
		}catch(e){
			console.log('error decoding token')
		}
	}


	next();
}

module.exports.error = function(err, req, res, next) {
    if (err) {
    	console.log('Error req.fullURL'.red, req.method, req.fullURL);
        console.log('[expectedError]'.red, err);
    	res.status(500).json({statusCode:500});
    }
}