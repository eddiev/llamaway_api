var express = require('express');
var _ = require('lodash');
var querystringify = require('querystringify');
var router = express.Router();
var User = require('../models/user');
var Auth = require('../models/auth');
var emails = require('../scripts/emails');
var Company = require('../models/company');
var searchClient = require('../scripts/search');
var utils = require('../scripts/utils');
var jwt = require('../scripts/jwt');
var userRequired = require('../middlewares/user-required');
var backofficeUserRequired = require('../middlewares/backoffice-user-required');
var BackofficeUser = require('../models/backofficeUser');

router.get('/health_check',function(req,res,next){
	res.json({ok:true})
});

router.post('/users/token',function(req,res,next){

    if(emails.validateFormat(req.body.email) && utils.validatePassword(req.body.password)){
	
		var query = {
	        email: req.body.email,
	        password: utils.sha1(req.body.password)
	    }
		Auth.findOne(query,function(err,foundUser){
			if(foundUser){
				var token = jwt.encode({
					_id: foundUser.uid
				});

				User.updateLastLogin(foundUser.uid);

				res.json({
					token:token
				});
			}else{
				res.status(403).json({
					statusCode:403,
					statusText:'wrong user or password'
				});
			}
		})
	}else{
		res.status(400).json({
			statusCode:400,
			statusText:'bad requets'
		});
	}
});

router.post('/users',function(req,res,next){
	if(emails.validateFormat(req.body.email) && utils.validatePassword(req.body.password)){
			var query = {
				email:req.body.email
			}

			User.findOne(query,function(err,user){
				if(user){
					res.status(409).json({statusCode:409});
				}else{
					var n = {
						email:req.body.email,
						password:utils.sha1(req.body.password),
						origin:utils.decodeOrigin(req.body.origin)
					}

					User.createUserAndAuth(n,function(err,newUser,newAuth){
						res.json(newUser);
					});
				}
			});
	}else{
		res.status(400).json({statusCode:400, error:'missing user or password or origin'});
	}
});

router.post('/users/experiment', function(req,res,next){
	if(emails.validateFormat(req.body.email)){
			var query = {
				email:req.body.email
			}
			console.log('Llego al endpoint')
			User.findOne(query,function(err,user){
				if(user){
					res.status(409).json({statusCode:409});
				}else{
					// Ahora envío un email para que todos seteen su contraseña
					console.log('No encontre usuario repetido')
					var n = {
						email:req.body.email,
						password:utils.sha1(req.body.password)
					}

					User.createUserAndAuth(n, function(err, newUser, newAuth){
						var params = querystringify.stringify({
							email:newAuth.email,
							recovery_token:newAuth.recovery_token,
							returnTo: req.get('origin') + '/ingreso/#/validatedUser'
						}, true);

						console.log('Usuario creado')
						var link = req._location.origin + '/auth/recoveryoken/'+params;

						// Necesito que el usuario ingrese su password! OJO CON ESTO QUE ESTA ANDANDO
						emails.sendWelcome({'email':newUser.email, 'validateLink':link});

						var token = jwt.encode({
							'_id': newAuth.uid
						});
						console.log('Token creado');

						User.updateLastLogin(newAuth.uid);

						res.json({
							'user':newUser,
							'token':token
						});
					});
				}
			});
	} else {
		res.status(400).json({statusCode:400, error:'missing user or password or origin'});
	}
});

router.post('/users/:email/recoverPassword',function(req,res,next){
	var query = {
		email:req.params.email
	}
	Auth.findOne(query,function(err,userAuth){
		if(userAuth){
			userAuth.recovery_token = utils.sha1((Math.random()*1234)+userAuth.email);

			userAuth.save(function(){
				// console.log(userAuth.recovery_token)
				var params = querystringify.stringify({
					email:userAuth.email,
					recovery_token:userAuth.recovery_token,
					returnTo:req.query.returnTo
				},true);
				
				var link = req._location.origin + '/auth/recoveryoken/'+params;
				
				emails.sendRecoverPassword({
					email:userAuth.email,
					link:link
				},function(emailRes){
					res.json({success:true});
				})
			})
		}else{
			res.status(404).json({statusCode:404});
		}
	})
});


router.get('/users/me',userRequired,function(req,res,next){
	User.findById(req._user._id,function(err,user){
		res.json(user);
	})
});

router.put('/users/me', userRequired, function(req,res,next){
    User.findById(req._user._id, '', function(err, user) {
		
		if(!err){
		    //Si cambio de email tengo que volver a validar
		    if(user.email != req.body.email){
		         user.validated = false;
		    }

		    user.email = req.body.email || user.email;

		    if(req.body.notificationConfig && req.body.notificationConfig.messageDaysFrequency){
		    	switch(req.body.notificationConfig.messageDaysFrequency){
		    		case '1':
		    			user.notificationConfig.messageDaysFrequency = 1;
		    		break;
		    		case '7':
		    			user.notificationConfig.messageDaysFrequency = 7;
		    		break;
		    		case '30':
		    			user.notificationConfig.messageDaysFrequency = 30;
		    		break;
		    		case '0':
		    			user.notificationConfig.messageDaysFrequency = 0;
		    		break;
		    	}
		    }

		    // Actualizo el user
		    user.save(function(err, user) {
		        if (!err) {
		        	if(user.companyId){
			        	Company.findById(user.companyId, '', function(err, company) {
					        if (!err) {
								company.name = req.body.company.name;
								company.contactName = req.body.company.contactName;
								company.phone = req.body.company.phone;

								company.save();

				                // Actualizo el Auth del usuario
				                var syncAuth = {
				                    email: user.email
				                }

				                Auth.findOneAndUpdate({
				                    uid: user._id
				                }, syncAuth, function() {});

				                res.json(user);
					        } else {
				                res.status(404).json({statusCode:404});
				            }
			        	});
		        	}else{
		        		res.json(user);
		        	}
		        } else {
		            if (err.code == "11001" || err.code == "11000") { //id duplicado
		                res.status(409).json({statusCode:409});
		            } else {
		                res.status(404).json({statusCode:404});
		            }
		        }
		    });
		} else {
			console.error('error al actualizar los datos', err);
			res.status(500).json({statusCode:500, error:'error al actualizar los datos'});
		}
    })
});

router.put('/users/password', userRequired, function(req,res,next){

	if(req.body.pd){
		var password = utils.sha1(req.body.pd);
        Auth.findOneAndUpdate({
            uid: req._user._id
        }, {password: password}, function() {
        	res.status(200).json({statusCode:200});
        });
    }else{
    	res.status(400).json({statusCode:400});
    }
});

router.post('/users/emailValidation', userRequired,function(req,res,next){
    Auth.findOne({'uid': req._user._id},function(err,userAuth){
        if(userAuth){
            userAuth.recovery_token = utils.sha1((Math.random()*1234)+userAuth.email);
            userAuth.recovery_token_created_at = Date.now();
            userAuth.save(function(){
                var params = querystringify.stringify({
                    email:userAuth.email,
                    recovery_token:userAuth.recovery_token,
                    returnTo: req.get('origin') + '/ingreso/#/validatedUser'
                },true);
                
                var link = req._location.origin + '/auth/recoveryoken/'+params;
                
                emails.sendEmailValidation({
                    'email':userAuth.email,
                    'validateLink':link
                },function(emailRes){
                    res.json({success:true});
                })

            })
        }else{
            res.status(404).json({statusCode:404});
        }
    })
});

/////////////////////////////////************************BACKOFFICE**************************////////////////////////////////
router.post('/users/llamatoken',function(req,res,next){

    if(emails.validateFormat(req.body.email) && utils.validatePassword(req.body.password)){
	
		var query = {
	        email: req.body.email,
	        password: utils.sha1(req.body.password)
	    }
		Auth.findOne(query,function(err,foundUser){
			if(foundUser){
				BackofficeUser.findOne({'uid':foundUser.uid}, function(err, doc){
					if(err){
						res.status(403).json({
							statusCode:403,
							statusText:'wrong user or password'
						});
					}else{
						if(doc){
							var token = jwt.encode({
								_id: doc.uid
							});

							res.json({
								token:token
							});
						}else{
							res.status(401).json({
								statusCode:401,
								statusText:'Unauthorize'
							});
						}
					}

				});

				
			}else{
				res.status(403).json({
					statusCode:403,
					statusText:'wrong user or password'
				});
			}
		})
	}else{
		res.status(400).json({
			statusCode:400,
			statusText:'bad requets'
		});
	}
});

router.put('/backoffice/users/me', backofficeUserRequired, function(req,res,next){
    User.findById(req.body.userId, '', function(err, user) {
		
		if(!err){
		    //Si cambio de email tengo que volver a validar
		    if(user.email != req.body.email){
		         user.validated = false;
		    }

		    user.email = req.body.email || user.email;

		    if(req.body.notificationConfig && req.body.notificationConfig.messageDaysFrequency){
		    	switch(req.body.notificationConfig.messageDaysFrequency){
		    		case '1':
		    			user.notificationConfig.messageDaysFrequency = 1;
		    		break;
		    		case '7':
		    			user.notificationConfig.messageDaysFrequency = 7;
		    		break;
		    		case '30':
		    			user.notificationConfig.messageDaysFrequency = 30;
		    		break;
		    		case '0':
		    			user.notificationConfig.messageDaysFrequency = 0;
		    		break;
		    	}
		    }

		    // Actualizo el user
		    user.save(function(err, user) {
		        if (!err) {
		        	if(user.companyId){
			        	Company.findById(user.companyId, '', function(err, company) {
					        if (!err) {
								company.name = req.body.company.name;
								company.contactName = req.body.company.contactName;
								company.phone = req.body.company.phone;

								company.save();

				                // Actualizo el Auth del usuario
				                var syncAuth = {
				                    email: user.email
				                }

				                Auth.findOneAndUpdate({
				                    uid: user._id
				                }, syncAuth, function() {});

				                res.json(user);
					        } else {
				                res.status(404).json({statusCode:404});
				            }
			        	});
		        	}else{
		        		res.json(user);
		        	}
		        } else {
		            if (err.code == "11001" || err.code == "11000") { //id duplicado
		                res.status(409).json({statusCode:409});
		            } else {
		                res.status(404).json({statusCode:404});
		            }
		        }
		    });
		} else {
			console.error('error al actualizar los datos', err);
			res.status(500).json({statusCode:500, error:'error al actualizar los datos'});
		}
    })
});

router.put('/backoffice/users/password', backofficeUserRequired, function(req,res,next){

	if(req.body.pd){
		var password = utils.sha1(req.body.pd);
        Auth.findOneAndUpdate({
            uid: req.body.userId
        }, {password: password}, function() {
        	res.status(200).json({statusCode:200});
        });
    }else{
    	res.status(400).json({statusCode:400});
    }
});

router.get('/backoffice/users/:companyId', backofficeUserRequired, function (req, res, next) {
	var query = {
		companyId: req.params.companyId
	}

	User.findOne(query, function(err,user) {
		res.json(user);
	});
});

module.exports = router;