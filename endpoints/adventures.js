var express = require('express');
var router = express.Router();
var HistoricAdventure = require('../models/historicadventure');
var Adventure = require('../models/adventure');
var TopPlace = require('../models/topPlace');
var TopAdventure = require('../models/topAdventure');
var Company = require('../models/company');
var User = require('../models/user');
var searchClient = require('../scripts/search');
var userRequired = require('../middlewares/user-required');
var backofficeUserRequired = require('../middlewares/backoffice-user-required');

var utils = require('../scripts/utils');
var q = require('q');
var emails = require('../scripts/emails');

router.get('/companies/:companyId/adventures',function(req,res,next){
	var limit = 10;
	var forceLastCreated = false;
	var order = {
		'created_at': -1
	}

	if(req.query.limit && req.query.limit < 10){
		limit = req.query.limit;
	}

	if(req.query.forceLastCreated){
		order = {'created_at': -1} //Fuerzo el sólo el ordenamiento descendiente
	}

	var skip = req.query.page ? req.query.page*limit : 0;

	Adventure.find({'companyId': req.params.companyId}, '', {'lean':true}, function(err, adventures){
		if(adventures){
			utils.normalizeNames(adventures);
			res.json({
				results:adventures
			});
		}else{
			res.json({
				results:[]
			});
		}
	})
	.sort(order)
	.limit(limit)
	.skip(skip);
});

var listActivityType = [
    { id: 1, name: 'Acúatica' },
    { id: 2, name: 'Avistaje de aves' },
    { id: 3, name: 'Cabalgatas' },
    { id: 4, name: 'Camping' },
    { id: 5, name: 'Ciclismo' },
    { id: 6, name: 'Deportes Extremos' },
    { id: 7, name: 'Deportes Invernales' },
    { id: 8, name: 'Expediciones/Travesías' },
    { id: 9, name: 'Montañismo y Trekking' },
    { id: 10, name: 'Pesca' },
    { id: 11, name: 'Tours/Otros' }
];

var payMethods = [
    { id: 1, name: 'Efectivo' },
    { id: 2, name: 'Tarjeta de débito' },
    { id: 3, name: 'Tarjeta de crédito' },
    { id: 4, name: 'Mercadopago/Transferencias'}
];

router.post('/companies/:companyId/adventures', userRequired, function(req,res,next){
	var newAdventure = new Adventure();

	newAdventure.name = req.body.name;
	newAdventure.description = req.body.description;
	newAdventure.golocation = JSON.parse(req.body.golocation);
	newAdventure.place = req.body.place;
	newAdventure.takeDays = req.body.takeDays;
	newAdventure.duration = req.body.duration;
	newAdventure.difficulty = req.body.difficulty;
	if(req.body.price && !isNaN(req.body.price) && req.body.withoutPrice != 1){
		newAdventure.price = req.body.price;
	}
	if(req.body.currency && req.body.withoutPrice == 0){
		newAdventure.currency = req.body.currency;
	}

	newAdventure._geoloc.lat = newAdventure.golocation.location.lat;
	newAdventure._geoloc.lng = newAdventure.golocation.location.lng;

	newAdventure.companyId = req.params.companyId;
	newAdventure.ownerId = req._user._id;

	if(req.body.activityType){
		var activityTypes = JSON.parse(req.body.activityType);
		var actualActivity;
		if(Array.isArray(activityTypes)){
			activityTypes.forEach(function(currentActivity) {
			    actualActivity = {
			        'id': currentActivity.id
			    }
			    listActivityType.some(function(currentList){
			    	if(currentList.id == currentActivity.id){
			    		actualActivity.name = currentList.name;
			    		return true;
			    	}
			    	return false;
			    });
			    newAdventure.activityType.push(actualActivity);
			});
		}else{
			newAdventure.activityType = [activityTypes];
		}
	}

	if(req.body.payMethod){
		var selectedPayMethods = JSON.parse(req.body.payMethod);
		if(Array.isArray(selectedPayMethods)){
			selectedPayMethods.forEach(function(item) {
				var currentPayMethod;
			    currentPayMethod = {
			        'id': item.id
			    }
			    payMethods.some(function(currentList){
			    	if(currentList.id == item.id){
			    		currentPayMethod.name = currentList.name;
			    		return true;
			    	}
			    	return false;
			    });
			    newAdventure.payMethod.push(currentPayMethod);
			});
		}
	}

	if (newAdventure.name && newAdventure.activityType && newAdventure.description && newAdventure.golocation && newAdventure._geoloc
		&& newAdventure.place && newAdventure.takeDays !==undefined && newAdventure.duration !==undefined &&
		newAdventure.difficulty !==undefined  && (newAdventure.currency !==undefined || req.body.withoutPrice != 0) && (newAdventure.payMethod !==undefined || req.body.withoutPrice == 2)  && (newAdventure.price !== undefined || req.body.withoutPrice == 1)){

		if(req.body.startDate != 'undefined'){
			newAdventure.startDate = req.body.startDate;
		}
		if(req.body.finishDate != 'undefined'){
			newAdventure.finishDate =  req.body.finishDate;
		}

		var updateCompany = {
			$push: {'adventures': newAdventure},
			$inc:{'adventuresCount':1}
		}

		newAdventure.save(function(err, newAdv){
			if(!err){
				Company.findByIdAndUpdate(newAdv.companyId, updateCompany, {'new':true}, function(err, newComp){
					if(err){
				  		console.error('Error to update company '+ newAdv.companyId + ': ', err);
				  		console.error('Stack: ', err.stack);
						res.status(500).json({statusCode:500, error:'Error to update company '+ newAdv.companyId});
					}else{
						searchClient.indexCompany(newComp.toJSON());
						var obj = newAdv.toObject();
				  		obj.safeUrlName = utils.normalizeName(newAdv.name);
				  		console.log('Adventure uploaded ok');
						searchClient.indexAdventure(newAdv.toJSON());
						res.json(obj);
						
					}
				});

			}else{
				console.error('error al crear la aventura', err);
				res.status(500).json({statusCode:500, error:'error al crear la aventura'});
			}
		});
	} else {
		res.status(400).json({statusCode:400, error:'Missing data'});
	}

});

router.put('/companies/:companyId/adventures/:adventureId', userRequired,function(req,res,next) {
	var query = {
		'companyId':req.params.companyId,
		'_id':req.params.adventureId,
		'ownerId':req._user._id
	}
	var valid = true;
	var adventure = {};
	if(req.body.payMethod != undefined && req.body.withoutPrice != 2){
		adventure['payMethod'] = req.body.payMethod;
	}else{
		adventure['payMethod'] = null;
	}
	if(req.body.price != undefined && !isNaN(req.body.price) && req.body.withoutPrice != 1){
		adventure['price'] = req.body.price;
	}else{
		adventure['price'] = null;
		if(req.body.withoutPrice == undefined){
			valid = false;
		}
	}

	if(req.body.currency != undefined && req.body.withoutPrice == 0){
		adventure['currency'] = req.body.currency;
	}else{
		adventure['currency'] = null;
	}

	if(req.body.takeDays !==undefined){
		adventure['takeDays'] = req.body.takeDays;
	}
	if(req.body.duration){
		adventure['duration'] = req.body.duration;
	}
	if(req.body.difficulty){
		adventure['difficulty'] = req.body.difficulty;
	}
	if(req.body.description ){
		adventure['description'] = req.body.description;
	}
	if(req.body.name){
		adventure['name'] =  req.body.name;
	}
	if(req.body.activityType){
		adventure['activityType'] =  req.body.activityType;
	}
	if(req.body.golocation){
		adventure['golocation'] =  req.body.golocation;
	}
	if(req.body.place){
		adventure['place'] =  req.body.place;
	}
	if(req.body._geoloc){
		adventure['_geoloc'] =  req.body._geoloc;
	}

	if(valid && adventure){
		adventure.updated_at = Date.now();
		Adventure.findOneAndUpdate(query, adventure, {'new':true}, function(err, newAdventure){
			if (err) {
				console.log(err);
			}

			if(newAdventure){
				newAdventure.safeUrlName = utils.normalizeName(newAdventure.name);
				searchClient.indexAdventure(newAdventure);
				res.json(newAdventure);
			}else{
				res.status(403).end();
			}
		})
	} else {
		res.status(400).json({statusCode:400, error:'Missing data'});
	}
});

router.post('/companies/:companyId/adventures/:adventureId/profile_picture', userRequired, function(req,res,next){
	if(req.files['profile_picture']){
		var file = req.files['profile_picture'].path;
		var companyId = req.params.companyId;
		var adventureId = req.params.adventureId;

		var query = {
			'_id':req.params.adventureId
		}

		Adventure.findOne(query, '',{'lean':true}, function(err, adventure){
			if(adventure){
				Adventure.uploadPicture(adventureId,file).then(function(responses) {
			  		adventure.safeUrlName = utils.normalizeName(adventure.name);
			  		adventure.profile_picture = responses.profile_picture;
			  		console.log('Adventure uploaded ok');
					searchClient.indexAdventure(adventure);
					res.json(adventure);
				}).catch(function(err){
			  		console.log('Adventure file upload error: ', err);
			  		console.log('Stack: ', err.stack);
					res.status(500).json({statusCode:500, error:'Adventure file upload error'});
				});
			}else{
				res.status(403).end();
			}
		});
	}else{
		res.status(400).json({error:'missing "profile_picture" field'});
	}
});

var deleteAdventurePicture = function(adventureId, pictureId){
	var defered = q.defer();
	Adventure.deletePicture(adventureId, pictureId, function (advResponse){
		if(advResponse){
			if(!advResponse.err){
				searchClient.indexAdventure(advResponse);
				defered.resolve(advResponse);
			} else {
				console.error(advResponse.err);
				defered.reject();
			}
		}else{
			defered.resolve();
		}
	});
	return defered.promise;
}

router.put('/companies/:companyId/adventures/:adventureId/profile_picture/:profile_pictureId', userRequired, function(req,res,next){
	if(req.files['profile_picture']){
		var newFile = req.files['profile_picture'].path;
		var adventureId = req.params.adventureId;
		var oldPictureId = req.params.profile_pictureId;

		var query = {
			'_id':req.params.adventureId
		}

		if(adventureId && oldPictureId){
			Adventure.findOne(query, '',{'lean':true}, function(err, adventure){
				if(adventure){
					Adventure.deletePicture(oldPictureId, function (err){
						console.log('Imagen eliminada');
						Adventure.uploadPicture(adventureId, newFile, oldPictureId).then(function(responses) {
					  		adventure.safeUrlName = utils.normalizeName(adventure.name);
					  		adventure.profile_picture = responses.profile_picture;
					  		console.log('Adventure uploaded ok');
							searchClient.indexAdventure(adventure);
							res.json(adventure);
						}).catch(function(err){
					  		console.log('Adventure file upload error: ', err);
					  		console.log('Stack: ', err.stack);
							res.status(500).json({statusCode:500, error:'Adventure file upload error'});
						});

					});
				}else{
					res.status(403).end();
				}
			});
		} else {
			res.status(400).json({statusCode:400, error:'Missing data'});
		}
	}else{
		res.status(400).json({error:'missing "profile_picture" field'});
	}
});

router.delete('/companies/:companyId/adventures/:adventureId/profile_picture/:profile_pictureId', userRequired, function(req,res,next){

	var ownerId = req._user._id;
	var companyId = req.params.companyId;
	var adventureId =  req.params.adventureId;
	var pictureId = req.params.profile_pictureId;

	if(pictureId && ownerId && companyId && pictureId){
		var query = {
			'companyId':companyId,
			'ownerId':ownerId,
			'_id':adventureId
		}

		Adventure.findOne(query, '', function(err, adventure){
			if(adventure){
				for(var i=0; i<adventure.profile_picture.length; i++){
					//Estoy completamente seguro de borrar el correcto
					if(adventure.profile_picture[i].imageId == pictureId){
						deleteAdventurePicture(adventureId, pictureId).then(function(){
							res.json(advResponse);	
						}).catch(function(){
							res.status(500).json({error:'fail'});
						});
					}
				}
			}else{
				res.status(403).end();
			}
		});
	} else {
		res.status(400).json({statusCode:400, error:'Missing data'});
	}

});

router.get('/adventures/search',function(req,res,next){
	var query = req.query;

	if(query.top_places){
		topAdventures().then(function(adventuresTop){
			var adventures = [];
			adventuresTop.forEach(function(current){
				adventures.push(current._id);
			});

			query.adventures = adventures.join();
			searchClient.searchAdventure(query).then(function(results) {
				utils.normalizeNames(results.results);
		        res.json(results);
		    }).catch(function(error) {
		        console.log(error.message);
		        res.status(400).json(results);
		    });
		});
	}else{
		searchClient.searchAdventure(query).then(function(results) {
			utils.normalizeNames(results.results);
	        res.json(results);
	    }).catch(function(error) {
	        console.log(error.message);
	        res.status(400).json(results);
	    });
	}
});

var topPlaceCache = {}
router.get('/adventures/topPlaces', function(req, res, next) {
	topPlaces().then(function(places){
    	res.json(places);
	});
});

var topPlaces = function(){
	var defered = q.defer();

	if (!topPlaceCache || !topPlaceCache.expire || topPlaceCache.expire >= Date.now()) {
        TopPlace.find({}, '', { 'lean': true }, function(err, adventures) {
            topPlaceCache.adventures = adventures;
            topPlaceCache.expire = Date.now() + (1000 * 60 * 60 * 24);

            defered.resolve(topPlaceCache.adventures);
        });
    } else {
        defered.resolve(topPlaceCache.adventures);
    }
	return defered.promise;
}

var topAdventureCache = {}
router.get('/adventures/top', function(req, res, next) {
	topAdventures().then(function(adventures){
    	res.json(adventures);
	});
});

var topAdventures = function(){
	var defered = q.defer();

    if (!topAdventureCache || !topAdventureCache.expire || topAdventureCache.expire >= Date.now()) {
        TopAdventure.find({}, 'adventureId', { 'lean': true }, function(err, adventuresIds) {
			var ids = [];
			adventuresIds.forEach(function(current){
				ids.push(current.adventureId);
			});
        	Adventure.find({_id:{'$in':ids}}, '', { 'lean': true },function(err, adventures) {
        		adventures.forEach(function(current){
					current.safeUrlName = utils.normalizeName(current.name);
				});
		        topAdventureCache.adventures = adventures;
		        topAdventureCache.expire = Date.now() + (1000 * 60 * 60 * 24);

            	defered.resolve(topAdventureCache.adventures);
        	});
        });
    } else {
    	defered.resolve(topAdventureCache.adventures);
    }
	return defered.promise;
}

router.get('/adventures/:adventureId',function(req,res,next){
	var query = {
		'_id':req.params.adventureId
	}

	Adventure.findOne(query, '',{'lean':true}, function(err, adventure){
		if(err){
			next(err);
		}else{
			if(adventure){
				adventure.safeUrlName = utils.normalizeName(adventure.name);
				res.json(adventure);
			}else{
				res.status(404).json({statusCode:404});
			}
		}
	});
});

router.delete('/companies/:companyId/adventures/:adventureId', userRequired, function (req,res,next) {
	var query = {
		'companyId': 	req.params.companyId,
		'_id': 		req.params.adventureId,
		'ownerId': 	req._user._id
	}

	// var body = Adventure.clean(req.body);
	// 	body.updated_at = Date.now();

	//Adventure.findOneAndRemove(query, function(error, adventure) {
	Adventure.findOneAndRemove(query).then(function(adventure){
		if (adventure) {
			var adventureJson = adventure.toJSON();
			delete adventureJson._id;
			var historicAdventure = new HistoricAdventure(adventureJson);
			historicAdventure.adventureId = adventure._id;
			historicAdventure.save().then(function(){
				searchClient.deleteAdventure(adventure._id.toString());
	
				adventure.profile_picture.forEach(function(currentImage) {
					deleteAdventurePicture(null, currentImage.imageId).then(function(){
						console.log("Deleted Image: " + currentImage.imageId);
					}).catch(function(){
						console.error("Error to delete image " + currentImage.imageId);
					})
				});

				var updateCompany = {
					$pull: {'adventures': adventure},
					$inc:{'adventuresCount':-1}
				}

				Company.findByIdAndUpdate(adventure.companyId, updateCompany, {'new':true}).then(function(company){
					searchClient.indexCompany(company.toJSON());
					res.json(historicAdventure);
				}).catch(function(err){
			  		console.error('Error to update company '+ company._id);
					if(err){
						console.error(err);
			  			console.error('Stack: ', err.stack);
					}
					res.status(500).json({statusCode:500, error:'Error to update company '+ company._id});
				});
			}).catch(function(err){
				console.log(err);
				res.status(500).json({statusCode:500, error:'Error to create historicAdventure'});
			});

		}else{
			res.status(403).end();
		}
	}).catch(function(err){
		console.error('Error to get adventure '+ req.params.adventureId + ': ', err);
		res.status(500).json({statusCode:500, error:'Error to delete adventure '+ adventure._id});
	});
});

///////////*************SOLO BACKOFFICE*********////////////////
router.put('/backoffice/companies/:companyId/adventures/:adventureId', backofficeUserRequired,function(req,res,next) {
	var query = {
		'companyId':req.params.companyId,
		'_id':req.params.adventureId
	}
	var valid = true;
	var adventure = {};
	if(req.body.payMethod != undefined && req.body.withoutPrice != 2){
		adventure['payMethod'] = req.body.payMethod;
	}else{
		adventure['payMethod'] = null;
	}
	if(req.body.price != undefined && !isNaN(req.body.price) && req.body.withoutPrice != 1){
		adventure['price'] = req.body.price;
	}else{
		adventure['price'] = null;
		if(req.body.withoutPrice == undefined){
			valid = false;
		}
	}

	if(req.body.currency != undefined && req.body.withoutPrice == 0){
		adventure['currency'] = req.body.currency;
	}else{
		adventure['currency'] = null;
	}

	if(req.body.takeDays !==undefined){
		adventure['takeDays'] = req.body.takeDays;
	}
	if(req.body.duration){
		adventure['duration'] = req.body.duration;
	}
	if(req.body.difficulty){
		adventure['difficulty'] = req.body.difficulty;
	}
	if(req.body.description ){
		adventure['description'] = req.body.description;
	}
	if(req.body.name){
		adventure['name'] =  req.body.name;
	}
	if(req.body.activityType){
		adventure['activityType'] =  req.body.activityType;
	}
	if(req.body.golocation){
		adventure['golocation'] =  req.body.golocation;
	}
	if(req.body.place){
		adventure['place'] =  req.body.place;
	}
	if(req.body._geoloc){
		adventure['_geoloc'] =  req.body._geoloc;
	}

	if(valid && adventure){
		adventure.updated_at = Date.now();
		Adventure.findOneAndUpdate(query, adventure, {'new':true}, function(err, newAdventure){
			if (err) {
				console.log(err);
			}

			if(newAdventure){
				newAdventure.safeUrlName = utils.normalizeName(newAdventure.name);
				searchClient.indexAdventure(newAdventure);
				res.json(newAdventure);
			}else{
				res.status(403).end();
			}
		})
	} else {
		res.status(400).json({statusCode:400, error:'Missing data'});
	}
});

router.delete('/backoffice/companies/:companyId/adventures/:adventureId', backofficeUserRequired, function (req,res,next) {
	var query = {
		'companyId': req.params.companyId,
		'_id': 		req.params.adventureId,
		'ownerId': 	req.body.userId
	}

	// var body = Adventure.clean(req.body);
	// 	body.updated_at = Date.now();
	Adventure.findOneAndRemove(query).then(function(adventure){
		if (adventure) {
			var adventureJson = adventure.toJSON();
			delete adventureJson._id;
			var historicAdventure = new HistoricAdventure(adventureJson);
			historicAdventure.adventureId = adventure._id;
			historicAdventure.save().then(function(){
				searchClient.deleteAdventure(adventure._id.toString());
	
				adventure.profile_picture.forEach(function(currentImage) {
					deleteAdventurePicture(null, currentImage.imageId).then(function(){
						console.log("Deleted Image: " + currentImage.imageId);
					}).catch(function(){
						console.error("Error to delete image " + currentImage.imageId);
					})
				});

				var updateCompany = {
					$pull: {'adventures': adventure},
					$inc:{'adventuresCount':-1}
				}

				Company.findByIdAndUpdate(adventure.companyId, updateCompany, {'new':true}).then(function(company){
					searchClient.indexCompany(company.toJSON());
					res.json(historicAdventure);
				}).catch(function(err){
			  		console.error('Error to update company '+ company._id);
					if(err){
						console.error(err);
			  			console.error('Stack: ', err.stack);
					}
					res.status(500).json({statusCode:500, error:'Error to update company '+ company._id});
				});
			}).catch(function(err){
				console.log(err);
				res.status(500).json({statusCode:500, error:'Error to create historicAdventure'});
			});

		}else{
			res.status(403).end();
		}
	}).catch(function(err){
		console.error('Error to get adventure '+ req.params.adventureId + ': ', err);
		res.status(500).json({statusCode:500, error:'Error to delete adventure '+ adventure._id});
	});
});

router.get('/backoffice/company/getAdventuresByEmail', backofficeUserRequired, function (req, res, next) {
	var query = {
		email: req.query.email
	}

	User.findOne(query, function(err,user) {
		if(err){
			res.status(404).end();
		}else{
			if(user){
				Adventure.find({companyId : user.companyId}, '', {'lean':true},function(err, adventures){
					if(adventures){
						res.json(adventures);
					}else{
						res.status(404).end();
					}
				});
			}else{
				res.status(204).end();
			}
		}
	});
});

router.delete('/backoffice/companies/:companyId/adventures/:adventureId/profile_picture/:profile_pictureId', backofficeUserRequired, function(req,res,next){

	var companyId = req.params.companyId;
	var adventureId =  req.params.adventureId;
	var pictureId = req.params.profile_pictureId;

	if(pictureId && companyId && pictureId){
		var query = {
			'companyId':companyId,
			'_id':adventureId
		}

		Adventure.findOne(query, '', function(err, adventure){
			if(adventure){
				for(var i=0; i<adventure.profile_picture.length; i++){
					//Estoy completamente seguro de borrar el correcto
					if(adventure.profile_picture[i].imageId == pictureId){
						deleteAdventurePicture(adventureId, pictureId).then(function(){
							res.json(advResponse);	
						}).catch(function(){
							res.status(500).json({error:'fail'});
						});
					}
				}
			}else{
				res.status(403).end();
			}
		});
	} else {
		res.status(400).json({statusCode:400, error:'Missing data'});
	}

});

module.exports = router;