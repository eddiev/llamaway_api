var express = require('express');
var router = express.Router();
var Lead = require('../models/lead');
var userRequired = require('../middlewares/user-required');
var Company = require('../models/company');
var emails = require('../scripts/emails');
var Package = require('../models/package');
var User = require('../models/user');
var Adventure = require('../models/adventure');

router.post('/leads/:companyId', userRequired, function(req, res, next) {

    if (req._user._id && req.params.companyId && req.body.email && req.body.contactName && req.body.phone && req.body.description) {
        var newLead = new Lead();

        newLead.userId = req._user._id;
        newLead.companyId = req.params.companyId;
        if (req.body.adventureId) {
            newLead.adventureId = req.body.adventureId;
        }
        newLead.email = req.body.email;
        newLead.contactName = req.body.contactName;
        newLead.phone = req.body.phone;
        newLead.description = req.body.description;

        newLead.contactDate = Date.now();

        User.findOne({ companyId: req.params.companyId }, '', { 'lean': true }, function(err, user) {
            if (err) {
                console.error(err);
                res.status(500).end();
            } else {
                if (!user) {
                    res.status(401).end();
                } else {
                    Company.findById(user.companyId, '+contactName +phone', function(err, company) {
                        if (!err) {
                            if (!company.leads || company.leads == 0) {
                                newLead.state = 'pending';
                            } else {
                                newLead.state = 'sended';
                                company.leads = company.leads - 1;
                            }
                            company.save();
                            Package.availablePackage(company._id, function(err, availablePackage) {
                                if(newLead.state == 'sended'){
                                  if (company.leads == Math.ceil(availablePackage.dischargeLeads * 0.1)) {
                                      emails.sendLowLeadsEmail({
                                          'email': user.email
                                      }, function() {});
                                  }
                                  if (req.body.adventureId) {
                                    Adventure.findById(req.body.adventureId, '', function(err, adventure){
                                      emails.sendContactEmail({
                                          'email': user.email,
                                          'companyPageName': company.pageName,
                                          'adventure': adventure,
                                          'contact_email': req.body.email,
                                          'contact_name': req.body.contactName,
                                          'telephone': req.body.phone,
                                          'description': req.body.description
                                      }, function() {});
                                    });
                                  }else{
                                    emails.sendContactEmail({
                                        'email': user.email,
                                        'contact_email': req.body.email,
                                        'contact_name': req.body.contactName,
                                        'telephone': req.body.phone,
                                        'description': req.body.description
                                    }, function() {});
                                  }
                                  emails.sendContactEmailLlamita({
                                      'companyName': company.name,
                                      'email': req.body.email,
                                      'contact_email': user.email,
                                      'contact_name':company.contactName,
                                      'telephone': company.phone,
                                      'location': company.golocation.formatted_address
                                  }, function(){});
                                }else{
                                  emails.sendPendingLeadEmail({
                                      'email': user.email
                                  }, function() {});

                                  emails.sendPendingLeadEmailLlamita({
                                      'companyName': company.name,
                                      'email': req.body.email,
                                  }, function(){});
                                }
                                newLead.save(function(err, lead) {
                                    if (err) {
                                        console.log(err);
                                        res.status(500).end();
                                    } else {
                                        res.json({ success: true });
                                    }
                                });
                            });
                        } else {
                            console.log(err);
                            res.status(500).end();
                        }
                    });
                }
            }
        });
    } else {
        res.status(400).end();
    }
});

module.exports = router;
