var express = require('express');
var router = express.Router();
var Package = require('../models/package');
var Company = require('../models/company');
var PackageTemplate = require('../models/packageTemplate');
var userRequired = require('../middlewares/user-required');
var User = require('../models/user');
var backofficeUserRequired = require('../middlewares/backoffice-user-required');

var cachePackage = {};

router.get('/packages', backofficeUserRequired, function(req, res, next) {
   /* if (cachePackage.obj && Date.now() - cachePackage.createdDate < 86400000) { //cache de un dia
        res.json(cachePackage.obj);
    } else {*/
        var option = {}
        if(req.query.state){
            option.state = req.query.state;
        }
        if(req.query.companyId){
            option.companyId = req.query.companyId;
        }
        Package.find(option, '', {'lean':true}, function(err, packages){
            console.log(option);
            if (err) {
                res.status(404).end();
            } else {
                if(packages){
                    res.json(packages);
                }else{
                    res.status(204).end();
                }
                //cachePackage.obj = docs.toJSON();
                //cachePackage.createdDate = Date.now(); 
                //res.json(cachePackage.obj);
            }
        });
    //}
});

router.get('/packages/available', function(req, res, next) {
    var currentDate = new Date().toISOString();
    var option = {
        $or:[{
            state: 1,
            exposition: {$in:[2,3,4]},
            dischargeDate:{
                $lte: currentDate
            },
            closeDate:{
                $gte: currentDate
            }  
        },{
            state: 1,
            exposition: 1,
            dischargeDate:{
                $lte: currentDate
            },
            closeDate:{
                $gte: currentDate
            } 
        }]
    }
    User.findOne({_id: req._user._id}, function(err, user){
        if(user && user.companyId){
            option.companyId = user.companyId;
            Package.findOne(option, '', {'lean':true}, function(err, availablePackage){
                if (err) {
                    console.error(err);
                    res.status(404).end();
                } else {
                    if(availablePackage){
                        res.json(availablePackage);
                    }else{
                        res.status(204).end();
                    }
                }
            });
        }else{
            res.status(401).end();
        }
    });
});

router.get('/packages/templates', function(req, res, next) {
   /* if (cachePackage.obj && Date.now() - cachePackage.createdDate < 86400000) { //cache de un dia
        res.json(cachePackage.obj);
    } else {*/
        PackageTemplate.find({}, '', {'lean':true}, function(err, packages){
            if (err) {
                console.error(err);
                res.status(404).end();
            } else {
                //cachePackage.obj = docs.toJSON();
                //cachePackage.createdDate = Date.now(); 
                //res.json(cachePackage.obj);
                res.json(packages);
            }
        });
    //}
});

//router.post('/packages/:packageId', function(req, res, next) {
router.post('/packages/:packageId', userRequired, function(req, res, next) {
    var packageId = req.params.packageId;
    var packageTime = req.params.time; //0: monthly - 1: yearly
    if(packageId){
        User.findOne({_id: req._user._id}, function(err, user){
            if(user && user.companyId){
                PackageTemplate.findOne({_id:packageId}, '', {'lean':true}, function(err, docs) {
                    
                    var packageObj = new Package();
                    packageObj.companyId = user.companyId;
                    packageObj.dischargeLeads = docs.leads;
                    packageObj.exposition = docs.exposition;

                    if(packageObj.exposition == 1){
                        packageObj.state = 1;
                        packageObj.dischargeDate = Date.now();
                        packageObj.closeDate = new Date(Date.now() + 86400000 * 30);
                    }else{
                        if(packageTime == 1 && docs.price.yearly){
                            packageObj.price = docs.price.yearly;
                            packageObj.period = 'yearly';
                        }else{
                            packageObj.price = docs.price.monthly;
                            packageObj.period = 'monthly';
                        }
                    }
                    packageObj.save(function(err, newPackage){
                        res.status(200).json(newPackage.toJSON());
                    });
                });
            }else{
                res.status(401).json({statusCode:401});
            }
        });
    }else{
        res.status(403).end();
    }
});

//router.post('/packages/:packageId', function(req, res, next) {
router.post('/packages/pay/:packageId', userRequired, function(req, res, next) {
    var packageId = req.params.packageId;
    if(packageId){

        Package.findOne({_id:packageId}, '', {'lean':true}, function(err, docPackage) {
            if(err){
                console.log('Error al obtener el paquete a pagar', err);
                res.status(500).end();
            }else{
                console.log('paquete a pagar');
                var options = {
                    dischargeDate: new Date().toISOString(),
                    state: 1
                }
                switch(docPackage.period){
                    case 'monthly':
                        options.closeDate = new Date(Date.now() + 86400000 * 30);
                    break;
                    case 'yearly':
                        options.closeDate = new Date(Date.now() + 86400000 * 365);
                    break;
                    default:
                        res.status(403).end();
                }
                Package.findOneAndUpdate({_id:packageId}, options, function(err, doc) {
                    if(doc){
                        Company.findOneAndUpdate({_id: doc.companyId}, {$inc:{leads:doc.dischargeLeads}}, function(err, company){
                            res.status(200).json(doc.toJSON());
                        });
                    }else{
                        res.status(500).end();
                    }
                });
            }
        });


    }else{
        res.status(403).end();
    }
});

module.exports = router;