var express = require('express');
var router = express.Router();
var passport = require('passport');
var moment = require('moment');

var Auth = require('../models/auth');
var utils = require('../scripts/utils');

require('../passport/facebook');
require('../passport/linkedin');
require('../passport/recovery_token');
require('../passport/token');

var urlLlamaWeb = process.env.urlLlamaWeb || 'http://localhost:8081';

var socialMiddleware = function(strategyName,callbackPath){
	return function(req,res,next){
		var params = (req.query.returnTo && ('?returnTo=' + encodeURIComponent(req.query.returnTo) + '&sl='+ req.query.sl)) || '?sl='+ req.query.sl;

		return passport.authenticate(strategyName, {
		    session: false,
		    callbackURL: req._location.origin + callbackPath + params,
		    failureRedirect: urlLlamaWeb + '/ingreso#/seleccion'
		})(req, res, next);
	}
} 

var successfulLoginMiddleware = function(req,res){
	if(!req.user.profileError){
		var user = {
			_id: req.user._id
		}
		var token = req.jwt.encode(user);
		var llamawayLoginCallbackURL = req.query.returnTo || urlLlamaWeb + '/loginCallback';

		if(req.select){
			llamawayLoginCallbackURL = urlLlamaWeb + '/ingreso#/seleccion'
		}
		var redirectURL = utils.appendParamToUrl(llamawayLoginCallbackURL,{
			name:'token',
			value:token
		});

		// add extra param with the create user state
		redirectURL = utils.appendParamToUrl(redirectURL,{
			name:'userCreated',
			value:!!req.userCreated
		})

		redirectURL = utils.appendParamToUrl(redirectURL,{
			name:'provider',
			value:req.provider || 'unknown'
		})

		res.redirect(redirectURL)
	} else {
		console.error('Error en signup de Facebook: ' + req.user.profileError)
		res.redirect(urlLlamaWeb + '/ingreso#/seleccion')
	}
}

// Facebook 
// var facebookCallbackPath = '/auth/facebook/callback';
var facebookAuthPath = '/auth/facebook';
var facebookMiddleware = socialMiddleware('facebook',facebookAuthPath);
router.get(facebookAuthPath, facebookMiddleware, successfulLoginMiddleware);

// Linkedin
var linkedinAuthPath = '/auth/linkedin';
var linkedinMiddleware = socialMiddleware('linkedin',linkedinAuthPath);
router.get(linkedinAuthPath,linkedinMiddleware,successfulLoginMiddleware);

// Token
router.get('/auth/token/:token', passport.authenticate('token'), successfulLoginMiddleware);

// Recovery Token
router.get('/auth/recoveryoken', passport.authenticate('recovery_token'), successfulLoginMiddleware);

module.exports = router;