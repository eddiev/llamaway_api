var express = require('express');
var _ = require('lodash');
var router = express.Router();
var request = require('request');
var Company = require('../models/company');
var userRequired = require('../middlewares/user-required');
var backofficeUserRequired = require('../middlewares/backoffice-user-required');
var utils = require('../scripts/utils');
var User = require('../models/user');
var searchClient = require('../scripts/search');
var Package = require('../models/package');
var PackageTemplate = require('../models/packageTemplate');

var listActivityType = [
    { id: 1, name: 'Acúatica' },
    { id: 2, name: 'Avistaje de aves' },
    { id: 3, name: 'Cabalgatas' },
    { id: 4, name: 'Camping' },
    { id: 5, name: 'Ciclismo' },
    { id: 6, name: 'Deportes Extremos' },
    { id: 7, name: 'Deportes Invernales' },
    { id: 8, name: 'Expediciones/Travesías' },
    { id: 9, name: 'Montañismo y Trekking' },
    { id: 10, name: 'Pesca' },
    { id: 11, name: 'Tours/Otros' }
];

router.get('/companies/verifyPageName', userRequired, function(req, res, next) {
	var query = req.query; 
	if(query.pageName && query.pageName.length > 0){
	    Company.findOne({'pageName':query.pageName}, function(err, company){
	        if (err) {
	            throw err;
	        } else {
	        	if(company){
	        		res.status(200).json({exists:true});
	        	}else{
	        		res.status(200).json({exists:false});
	        	}
	        }
	    });
	}
});


router.get('/companies/search',function(req,res,next){
	var query = req.query;

	searchClient.searchCompany(req.query).then(function(results) {
		utils.normalizeNames(results.results);
        res.json(results);
    }).catch(function(error) {
        console.log(error.message);
        res.status(400).json(results);
    });
});

/*
router.get('/companies', function(req, res, next) {
	var query = {};

    Company.find(query, '', {
        'lean': true
    }).sort('name').exec(function(err, companies) {
        if (err) {
            throw err;
        } else {
            utils.normalizeNames(companies);
            res.json(companies);
        }
    });
});
*/

router.post('/companies', userRequired,function(req,res,next){
	var newCompany = new Company();

	if(req.body.contactName && req.body.phone){
		newCompany.contactName = req.body.contactName;
		newCompany.phone = req.body.phone;
	}
    newCompany.save(function(err, newComp) {
        searchClient.indexCompany(newComp.toJSON());
        //Cuando creo la compañia, le asigno un usuario. Por el momento es relacion uno a uno
        User.findByIdAndUpdate(req._user._id, { 'companyId': newComp._id }, { 'new': true }, function(err, newUser) {
            res.json(newComp);
            PackageTemplate.findOne({ exposition: 3 }, '', { 'lean': true }, function(err, packageTemplate) {
                var packageObj = new Package();
                packageObj.companyId = newComp._id;
                packageObj.packageId = packageTemplate._id;
                packageObj.dischargeLeads = packageTemplate.leads;
                packageObj.exposition = packageTemplate.exposition;
                packageObj.state = 1;
                packageObj.dischargeDate = Date.now();
                packageObj.closeDate = new Date("11-01-2016");
                packageObj.save(function(err, newPackage) {
                    if(err){
                    	console.error(err);
                    }else{
                        Company.findOneAndUpdate({_id: newComp._id}, {$inc:{leads:packageTemplate.leads}}, function(err, company){
                            if(err){
                            	console.error(err);
                            }
                        });
                    }
                });
            });
        });
    });
});

router.put('/companies', userRequired,function(req,res,next){
	var newCompany = {}

	if(req.body.contactName && req.body.phone){
		newCompany.contactName = req.body.contactName;
		newCompany.phone = req.body.phone;
		newCompany.updated_at = Date.now();
	} else {
		res.status(400).json({'statusCode':400});
	}

	Company.findByIdAndUpdate({ownerId:req._user._id}, newCompany, {'new':true}, function(err, newComp){
		searchClient.indexCompany(newComp.toJSON());
	});
});

router.get('/companies/:companyId',function(req,res,next){
	Company.findById(req.params.companyId, '', {'lean':true},function(err, company){
		if(company){
			utils.normalizeNames([company]);
			res.json(company);
		}else{
			res.status(404).end();
		}
	});
});

router.get('/companies/:companyId/me', userRequired, function(req,res,next){
	var query = {
		'_id':req.params.companyId,
		'ownerId': req._user._id
	}
	Company.findById(req.params.companyId, '+contactName +phone', {'lean':true},function(err, company){

		if(company){
			utils.normalizeNames([company]);
			res.json(company);
		}else{
			res.status(404).end();
		}
	});
});

router.get('/companies/byPageName/:pageName', function(req,res,next){
	Company.findOne({'pageName':req.params.pageName}, '',{'lean':true},function(err, company){
		if(company){
			utils.normalizeNames([company]);
			res.json(company);
		}else{
			res.status(404).end();
		}
	});
});

router.get('/companies/:companyId/profile_picture',function(req,res,next){
	Company.findById(req.params.companyId, '', {'lean':true}, function(err,company){
		var url = company.profile_picture.url || 'http://lorempixel.com/200/200/animals/';
		var parsed = require('url').parse(url);
		proxy(parsed.host,{
		  forwardPath: function(req, res) {
		    return parsed.path;
		  }
		})(req,res,next)
	});
});

router.put('/companies/:companyId/profile_picture', userRequired, function(req,res,next){
	if(req.files['profile_picture']){
		var file = req.files['profile_picture'].path;
		var companyId = req.params.companyId;
		if(req.body.replacePicture){ //Cambio el logo desde backoffice
			Company.findOne({'_id':companyId}, function(error, company){
				if(company){
					Company.deletePicture(company, function (err){
						console.log('Imagen eliminada');
						Company.uploadPicture(companyId,file,function (err, company){
		  					res.json(company);
						});
					});
				}else{
					res.status(403).end();
				}
			})
		} else {
			Company.uploadPicture(companyId,file,function (err, company){
		  		res.json(company);
			});
		}
	}else{
		res.status(400).json({error:'missing "profile_picture" field'});
	}
});

router.put('/companies/page', userRequired, function(req,res,next){
	User.findById(req._user._id,function(err,user){
		if(!err){
			Company.findById(user.companyId, '+contactName +phone', function(err, company) {
		        if (!err) {
					company.name = req.body.name;
					company.pageName = req.body.pageName;
					company.golocation = req.body.golocation;
					company.description = req.body.description;
					company._geoloc = req.body._geoloc;
					company.updated_at = Date.now();

					if(req.body.activities){
						var activityTypes = req.body.activities;
						var actualActivity;
						if(Array.isArray(activityTypes)){
							company.activities = [];
							activityTypes.forEach(function(currentActivity) {
							    actualActivity = {
							        'id': currentActivity.id
							    }
							    listActivityType.some(function(currentList){
							    	if(currentList.id == currentActivity.id){
							    		actualActivity.name = currentList.name;
							    		return true;
							    	}
							    	return false;
							    });
							    company.activities.push(actualActivity);
							});
						}
					}
					company.save(function(err, newComp){
				        if (err){
				        	console.log(err);
				            res.status(404).json({statusCode:404});
				        }else{
				        	res.json(newComp);
				        	delete company._doc.contactName;
				        	delete company._doc.phone;
				        	searchClient.indexCompany(company.toJSON());
				        }
					});
		        }else{
		        	res.status(404).json({statusCode:404});
		        }
			});
		}else{
			res.status(404).json({statusCode:404});
		}
	});
});


////////////////**************************BACKOFFICE*************************////////////////////


router.put('/backoffice/companies/page', backofficeUserRequired, function(req,res,next){
	Company.findById(req.body.companyId, '+contactName +phone', function(err, company) {
        if (!err) {
			company.name = req.body.name;
			company.pageName = req.body.pageName;
			company.golocation = req.body.golocation;
			company.description = req.body.description;
			company._geoloc = req.body._geoloc;

			if(req.body.activities){
				var activityTypes = req.body.activities;
				var actualActivity;
				if(Array.isArray(activityTypes)){
					company.activities = [];
					activityTypes.forEach(function(currentActivity) {
					    actualActivity = {
					        'id': currentActivity.id
					    }
					    listActivityType.some(function(currentList){
					    	if(currentList.id == currentActivity.id){
					    		actualActivity.name = currentList.name;
					    		return true;
					    	}
					    	return false;
					    });
					    company.activities.push(actualActivity);
					});
				}
			}
			company.save(function(err, newComp){
		        if (err){
		        	console.log(err);
		            res.status(404).json({statusCode:404});
		        }else{
		        	res.json(newComp);
		        	delete company._doc.contactName;
		        	delete company._doc.phone;
		        	searchClient.indexCompany(company.toJSON());
		        }
			});
        }else{
        	res.status(404).json({statusCode:404});
        }
	});
});

router.get('/backoffice/companies/:companyId', userRequired, function(req,res,next){
	var query = {
		'_id':req.params.companyId,
		'ownerId': req.body.userId
	}
	Company.findById(req.params.companyId, '+contactName +phone', {'lean':true},function(err, company){

		if(company){
			utils.normalizeNames([company]);
			res.json(company);
		}else{
			res.status(404).end();
		}
	});
});

router.get('/backoffice/company/byEmail', backofficeUserRequired, function (req, res, next) {
	var query = {
		email: req.query.email
	}

	User.findOne(query, function(err,user) {
		if(err){
			res.status(404).end();
		}else{
			if(user){
				Company.findById(user.companyId, '', {'lean':true},function(err, company){
					if(company){
						utils.normalizeNames([company]);
						company.user = user;
						res.json(company);
					}else{
						res.status(404).end();
					}
				});
			}else{
				res.status(204).end();
			}
		}
	});
});

module.exports = router;