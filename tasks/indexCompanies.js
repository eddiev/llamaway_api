var Adventure = require('../models/adventure');
var Company = require('../models/company');
var search = require('../scripts/search');

var updatedCompanies = 0;
var updatedAdventures = 0;

exports.run = function (argument) {
	Company.find({}).stream()
		.on('data', function (doc){
			console.log("Company: " + doc._id);
			search.indexCompany(doc.toJSON());
		})
		.on('close', function (){
		    Adventure.find({}).stream()
			.on('data', function (doc){
				console.log("Adventure: " + doc._id);
				search.indexAdventure(doc.toJSON());
			})
			.on('close', function (){
			    console.log('close stream index');
			});	
		});	
}

this.run()