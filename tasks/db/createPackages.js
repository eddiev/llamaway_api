var PackageTemplate = require('../../models/packageTemplate');
var async = require('async');

var packages = [
	{
		title: 'Gratis',
		leads: 10,
		state: 1,
		exposition: 1,
		order: 1,
		mpId: 'prueba'
	},
	{
		title: 'Llamita',
		leads: 30,
		state: 1,
		exposition: 2,
		price: {
			monthly: 10,
		},
		order: 2,
		mpId: 'prueba'
	},
	{
		title: 'Llama de las sierras',
		leads: 75,
		state: 1,
		exposition: 3,
		price: {
			monthly: 20,
			yearly: 200
		},
		order: 3,
		mpId: 'prueba'
	},
	{
		title: 'Llama dorada',
		leads: 300,
		state: 1,
		exposition: 4,
		price: {
			monthly: 50,
			yearly: 500
		},
		order: 4,
		mpId: 'prueba'
	}
]

exports.run = function (argument) {
	async.eachSeries(packages, function(current, callback) {
		var packageTemplate = new PackageTemplate();
		Object.getOwnPropertyNames(current).forEach(function(val, idx, array) {
			packageTemplate[val] = current[val];
		});
		packageTemplate.save(function(){
			callback();
		});
	}, function(err){
        if(err) {
			console.log('Error');
			console.error(err);
        }else{
        	console.log('Paquetes agregados exitosamente.');
        }
        process.exit(0);
    });
}

this.run()