require('newrelic');

var express = require('express');
var cors = require('cors');
var multer  = require('multer')
var passport  = require('passport')
var cookieParser = require('cookie-parser');
var session = require('express-session');

var expressUtils = require('./middlewares/express-utils');
var security = require('./middlewares/security');

var bodyParser = require('body-parser');

var compression = require('compression');

require('./ascii');

var app = express();
var colors = require('colors')
var port = process.env.PORT || 3042;

app.set('trust proxy',true);

app.use(compression());

app.use(cors());
app.use(bodyParser.json());
app.use(multer());
app.use(cookieParser('-$vivaperoncarajo$-'));
app.use(session({ 
	secret: '-$vivaperoncarajo$-', 
	resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(expressUtils.success);
app.use(expressUtils.error);
app.use(security);

app.options('*', cors());
app.use(require('./endpoints/auth'));
app.use(require('./endpoints/users'));
app.use(require('./endpoints/companies'));
app.use(require('./endpoints/adventures'));
app.use(require('./endpoints/leads'));
app.use(require('./endpoints/packages'));

app.listen(port,function () {
	console.log('api running on http://localhost:' + port);
});

process.on('uncaughtException', function(err) {
    console.log("[uncaughtException]".red, err, err.stack);
});
